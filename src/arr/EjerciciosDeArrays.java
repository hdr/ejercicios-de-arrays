package arr;

import java.util.Scanner;

public class EjerciciosDeArrays {

	// Parte I
	//
	// 1. Escribir la función `public static void imprimirArray(int[] a)`
	// que recibe un array de enteros y muestra el contenido del mismo
	// por pantalla.
	public static void imprimirArray(int[] a) {
		String s = "[ ";
		for (int i = 0; i < a.length; i++) {
			s += a[i] + " ";
		}
		s += "]";
		System.out.println(s);
	}

	// 2. Escribir la función `public static int[] pedirArray(int n)`
	// que construye un array de longitud `n`, solicitando a le usuarie
	// el valor de cada una de las posiciones y devuelve el array
	// construído.
	public static int[] pedirArray(int n) {
		Scanner input = new Scanner(System.in);
		int[] a = new int[n];
		for (int i = 0; i < a.length; i++) {
			System.out.print(">>> ");
			a[i] = input.nextInt();
		}
		input.close();
		return a;
	}

	// 3. Escribir la función `public static int[] rango(int m, int n)`
	// que recibe dos enteros `m` y `n`, y devuelve un array que
	// contiene todos los números comprendidos en el intervalo
	// `[m, n]` (inclusive). Probar la función con varios ejemplos.
	public static int[] rango(int m, int n) {
		int[] a = new int[n - m + 1];
		for (int i = m; i <= n; i++) {
			a[i - m] = i;
		}
		return a;
	}

	// 4. Implementar una función `public static double promedio(int[] a)`
	// que dado un array de enteros, devuelva el promedio de los
	// elementos del array.
	public static double prom(int[] a) {
		int sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i];
		}
		return (double) sum / a.length;
	}

	// 5. Implementar una función `public static int max(int[] a)`, que
	// dado un array de enteros desordenado devuelva el máximo elemento
	// del array.
	public static int max(int[] a) {
		int m = a[0];
		for (int i = 0; i < a.length; i++) {
			if (a[i] > m) {
				m = a[i];
			}
		}
		return m;
	}

	// 6. Implementar una función `public static int[] reverso(int[] a)`,
	// que dado un array de enteros devuelva un nuevo array con los
	// elementos en orden inverso.
	public static int[] reverso(int[] a) {
		int[] r = new int[a.length];
		for (int i = 0; i < a.length; i++) {
			r[i] = a[a.length - 1 - i];
		}
		return r;
	}

	// Parte II
	//
	// 1. `public static int[] agregarAtras(int[] a, int item)`
	//
	// Devuelve un array que tiene los mismos elementos que el array
	// recibido, con el nuevo elemento `item` agregado al final.
	// TODO
	public static int[] agregarAtras(int[] a, int item) {
		return null;
	}

	// 2. `public static int[] quitar(int[] a, int i)`
	//
	// Devuelve un array que tiene los mismos elementos que el array
	// recibido, excepto el elemento que estaba en la posición `i`.
	// TODO
	public static int[] quitar(int[] a, int i) {
		return null;
	}

	// 3. `public static int[] opuestos(int[] a)`
	//
	// Devuelve un array con los elementos opuestos, e.g: si el valor
	// es 2 devuelve -2.
	// TODO
	public static int[] opuestos(int[] a) {
		return null;
	}

	// 4. `public static boolean sonTodosDivisoresDe(int[] a, int n)`
	//
	// Devuelve `true` si todos los elementos del array recibido son
	// divisores de `n`.
	// TODO
	public static boolean sonTodosDivisoresDe(int[] a, int n) {
		return false;
	}

	// 5. Implementar la función `public static int cantidadDeRepetidos(int[] a)`
	// que dado un array, devuelve la cantidad de elementos repetidos.
	// TODO
	public static int cantidadDeRepetidos(int[] a) {
		return 0;
	}

	// 6. Implementar la función `public static int[] sinRepetidos(int[] a)`
	// que dado un array, devuelve un nuevo array sin los elementos
	// repetidos. Utilizar la función `cantidadDeRepetidos()` para saber
	// la cantidad de elementos del nuevo array.
	// TODO
	public static int[] sinRepetidos(int[] a) {
		return null;
	}

	public static void main(String[] args) {
		String s = "Viva Perón"; // new oculto
		int[] a = { 1, 2, 3 };   // new oculto
		
		// imprimirArray(a);
		
		int k = 1;
		int[][] m = new int[3][4];
		
		// m.length == 3
		// m[0].length == 4
		
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				m[i][j] = k++;
			}
		}
		
		imprimirArray(m[0]);
		imprimirArray(m[1]);
		imprimirArray(m[2]);
		
		System.out.println("a: " + a);
		System.out.println("a[0]: " + a[0]);
		System.out.println("m: " + m);
		System.out.println("m[0]: " + m[0]);

	}

}
